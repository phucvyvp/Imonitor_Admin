package com.datasection.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datasection.model.Topic;

public interface TopicRepository extends JpaRepository<Topic, Integer>{

	public Topic findTopicById(int id);
}
