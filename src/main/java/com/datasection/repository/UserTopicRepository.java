package com.datasection.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datasection.model.UserTopic;

public interface UserTopicRepository extends JpaRepository<UserTopic, Integer>{

}
