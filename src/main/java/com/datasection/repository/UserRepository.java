package com.datasection.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.datasection.model.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	 public User findByEmail(String email);
	 
	 public User findUSerById(int id);
	 public List<User> findUserByUsername(String usernames);
	
}
