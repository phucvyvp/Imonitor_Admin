package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the user_topic_view database table.
 * 
 */
@Entity
@Table(name="user_topic_view")
@NamedQuery(name="UserTopicView.findAll", query="SELECT u FROM UserTopicView u")
public class UserTopicView implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="number_of_views")
	private int numberOfViews;

	@Column(name="topic_id")
	private int topicId;

	@Column(name="user_id")
	private int userId;

	private String username;

	@Temporal(TemporalType.DATE)
	@Column(name="view_date")
	private Date viewDate;

	public UserTopicView() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNumberOfViews() {
		return this.numberOfViews;
	}

	public void setNumberOfViews(int numberOfViews) {
		this.numberOfViews = numberOfViews;
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getViewDate() {
		return this.viewDate;
	}

	public void setViewDate(Date viewDate) {
		this.viewDate = viewDate;
	}

}