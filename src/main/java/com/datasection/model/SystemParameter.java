package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the system_parameter database table.
 * 
 */
@Entity
@Table(name="system_parameter")
@NamedQuery(name="SystemParameter.findAll", query="SELECT s FROM SystemParameter s")
public class SystemParameter implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String description;

	private String key;

	private int status;

	private String type;

	private String value;

	public SystemParameter() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getKey() {
		return this.key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return this.value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}