package com.datasection.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;




/**
 * The persistent class for the topic database table.
 * 
 */
@Entity
@Table(name="topic")
@NamedQuery(name="Topic.findAll", query="SELECT t FROM Topic t")
public class Topic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="created_at")
	private int createdAt;

	private byte flag;

	private String media;

	private String mustwords;

	private String name;

	private String shouldwords;

	private String stopwords;
	
	//bi-directional many-to-one association to UserTopic
		//@OneToMany(mappedBy="topic")
//	@OneToMany(mappedBy="topic")
//	private List<UserTopic> userTopic;

	public Topic() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(int createdAt) {
		this.createdAt = createdAt;
	}

	public byte getFlag() {
		return this.flag;
	}

	public void setFlag(byte flag) {
		this.flag = flag;
	}

	public String getMedia() {
		return this.media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getMustwords() {
		return this.mustwords;
	}

	public void setMustwords(String mustwords) {
		this.mustwords = mustwords;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShouldwords() {
		return this.shouldwords;
	}

	public void setShouldwords(String shouldwords) {
		this.shouldwords = shouldwords;
	}

	public String getStopwords() {
		return this.stopwords;
	}

	public void setStopwords(String stopwords) {
		this.stopwords = stopwords;
	}

	//get-set usertopic
	
//	public List<UserTopic> getUserTopic() {
//		return userTopic;
//	}
//
//	public void setUserTopic(List<UserTopic> userTopic) {
//		this.userTopic = userTopic;
//	}
	

}