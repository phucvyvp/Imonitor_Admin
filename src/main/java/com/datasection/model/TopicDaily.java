package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the topic_daily database table.
 * 
 */
@Entity
@Table(name="topic_daily")
@NamedQuery(name="TopicDaily.findAll", query="SELECT t FROM TopicDaily t")
public class TopicDaily implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="auto_sentimen")
	private byte autoSentimen;

	@Column(name="auto_tag")
	private byte autoTag;

	private int inday;

	@Column(name="topic_id")
	private int topicId;

	@Column(name="total_post")
	private int totalPost;

	@Column(name="use_flag")
	private int useFlag;

	public TopicDaily() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getAutoSentimen() {
		return this.autoSentimen;
	}

	public void setAutoSentimen(byte autoSentimen) {
		this.autoSentimen = autoSentimen;
	}

	public byte getAutoTag() {
		return this.autoTag;
	}

	public void setAutoTag(byte autoTag) {
		this.autoTag = autoTag;
	}

	public int getInday() {
		return this.inday;
	}

	public void setInday(int inday) {
		this.inday = inday;
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getTotalPost() {
		return this.totalPost;
	}

	public void setTotalPost(int totalPost) {
		this.totalPost = totalPost;
	}

	public int getUseFlag() {
		return this.useFlag;
	}

	public void setUseFlag(int useFlag) {
		this.useFlag = useFlag;
	}

}