package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the topic_tagname database table.
 * 
 */
@Entity
@Table(name="topic_tagname")
@NamedQuery(name="TopicTagname.findAll", query="SELECT t FROM TopicTagname t")
public class TopicTagname implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="tag_id")
	private int tagId;

	@Column(name="tag_key")
	private String tagKey;

	@Column(name="tag_name")
	private String tagName;

	@Column(name="topic_id")
	private int topicId;

	public TopicTagname() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTagId() {
		return this.tagId;
	}

	public void setTagId(int tagId) {
		this.tagId = tagId;
	}

	public String getTagKey() {
		return this.tagKey;
	}

	public void setTagKey(String tagKey) {
		this.tagKey = tagKey;
	}

	public String getTagName() {
		return this.tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

}