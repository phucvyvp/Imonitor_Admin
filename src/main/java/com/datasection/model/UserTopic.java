package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_topic database table.
 * 
 */
@Entity
@Table(name="user_topic")
@NamedQuery(name="UserTopic.findAll", query="SELECT u FROM UserTopic u")
public class UserTopic implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private int flag;
	
//	@ManyToOne
//	private User user;
//	
//	@ManyToOne
//	private Topic topic;
//	
	
	
//	@ManyToOne
//	@JoinColumn(name="topic_id")
	@Column(name="topic_id")
	private int topicId;

//	@ManyToOne
//	@JoinColumn(name="user_id")
	@Column(name="user_id")
	private int userId;
	
	

	public UserTopic() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFlag() {
		return this.flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}