package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the user_login_history database table.
 * 
 */
@Entity
@Table(name="user_login_history")
@NamedQuery(name="UserLoginHistory.findAll", query="SELECT u FROM UserLoginHistory u")
public class UserLoginHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String ip;

	@Column(name="login_at")
	private Timestamp loginAt;

	@Column(name="user_id")
	private int userId;

	public UserLoginHistory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Timestamp getLoginAt() {
		return this.loginAt;
	}

	public void setLoginAt(Timestamp loginAt) {
		this.loginAt = loginAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}