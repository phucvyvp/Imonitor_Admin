package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the topic_audit database table.
 * 
 */
@Entity
@Table(name="topic_audit")
@NamedQuery(name="TopicAudit.findAll", query="SELECT t FROM TopicAudit t")
public class TopicAudit implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="created_at")
	private int createdAt;

	private String media;

	private String mustwords;

	private String name;

	private String operation;

	private String shouldwords;

	private String stopwords;

	@Column(name="topic_id")
	private int topicId;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	public TopicAudit() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(int createdAt) {
		this.createdAt = createdAt;
	}

	public String getMedia() {
		return this.media;
	}

	public void setMedia(String media) {
		this.media = media;
	}

	public String getMustwords() {
		return this.mustwords;
	}

	public void setMustwords(String mustwords) {
		this.mustwords = mustwords;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getShouldwords() {
		return this.shouldwords;
	}

	public void setShouldwords(String shouldwords) {
		this.shouldwords = shouldwords;
	}

	public String getStopwords() {
		return this.stopwords;
	}

	public void setStopwords(String stopwords) {
		this.stopwords = stopwords;
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

}