package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the status_meaning database table.
 * 
 */
@Entity
@Table(name="status_meaning")
@NamedQuery(name="StatusMeaning.findAll", query="SELECT s FROM StatusMeaning s")
public class StatusMeaning implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int status;

	private String meaning;

	public StatusMeaning() {
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMeaning() {
		return this.meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

}