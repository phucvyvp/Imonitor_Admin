package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the recovery_user database table.
 * 
 */
@Entity
@Table(name="recovery_user")
@NamedQuery(name="RecoveryUser.findAll", query="SELECT r FROM RecoveryUser r")
public class RecoveryUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String code;

	private int flag;

	@Column(name="make_time")
	private int makeTime;

	@Column(name="user_id")
	private int userId;

	public RecoveryUser() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getFlag() {
		return this.flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public int getMakeTime() {
		return this.makeTime;
	}

	public void setMakeTime(int makeTime) {
		this.makeTime = makeTime;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}