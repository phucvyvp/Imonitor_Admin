package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the unsubcribe database table.
 * 
 */
@Entity
@Table(name="unsubcribe")
@NamedQuery(name="Unsubcribe.findAll", query="SELECT u FROM Unsubcribe u")
public class Unsubcribe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	private int userId;

	public Unsubcribe() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

}