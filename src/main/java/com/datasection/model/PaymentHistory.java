package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the payment_history database table.
 * 
 */
@Entity
@Table(name="payment_history")
@NamedQuery(name="PaymentHistory.findAll", query="SELECT p FROM PaymentHistory p")
public class PaymentHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String coupon;

	@Column(name="created_at")
	private Timestamp createdAt;

	@Lob
	private String description;

	@Column(name="discount_coupon")
	private int discountCoupon;

	@Column(name="discount_time")
	private byte discountTime;

	private byte month;

	@Column(name="order_code")
	private String orderCode;

	@Column(name="payment_total")
	private int paymentTotal;

	@Column(name="plan_id")
	private int planId;

	private int status;

	@Lob
	@Column(name="transaction_message")
	private String transactionMessage;

	@Column(name="updated_at")
	private Timestamp updatedAt;

	@Column(name="user_id")
	private int userId;

	@Column(name="user_name")
	private String userName;

	public PaymentHistory() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCoupon() {
		return this.coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDiscountCoupon() {
		return this.discountCoupon;
	}

	public void setDiscountCoupon(int discountCoupon) {
		this.discountCoupon = discountCoupon;
	}

	public byte getDiscountTime() {
		return this.discountTime;
	}

	public void setDiscountTime(byte discountTime) {
		this.discountTime = discountTime;
	}

	public byte getMonth() {
		return this.month;
	}

	public void setMonth(byte month) {
		this.month = month;
	}

	public String getOrderCode() {
		return this.orderCode;
	}

	public void setOrderCode(String orderCode) {
		this.orderCode = orderCode;
	}

	public int getPaymentTotal() {
		return this.paymentTotal;
	}

	public void setPaymentTotal(int paymentTotal) {
		this.paymentTotal = paymentTotal;
	}

	public int getPlanId() {
		return this.planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTransactionMessage() {
		return this.transactionMessage;
	}

	public void setTransactionMessage(String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}

	public Timestamp getUpdatedAt() {
		return this.updatedAt;
	}

	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}