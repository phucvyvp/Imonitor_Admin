package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the topic_sentimen database table.
 * 
 */
@Entity
@Table(name="topic_sentimen")
@NamedQuery(name="TopicSentimen.findAll", query="SELECT t FROM TopicSentimen t")
public class TopicSentimen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="sentimen_key")
	private String sentimenKey;

	@Column(name="sentimen_tag")
	private int sentimenTag;

	@Column(name="topic_id")
	private int topicId;

	public TopicSentimen() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSentimenKey() {
		return this.sentimenKey;
	}

	public void setSentimenKey(String sentimenKey) {
		this.sentimenKey = sentimenKey;
	}

	public int getSentimenTag() {
		return this.sentimenTag;
	}

	public void setSentimenTag(int sentimenTag) {
		this.sentimenTag = sentimenTag;
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

}