package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mention database table.
 * 
 */
@Entity
@Table(name="mention")
@NamedQuery(name="Mention.findAll", query="SELECT m FROM Mention m")
public class Mention implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	@Column(name="free_tag")
	private String freeTag;

	private int inday;

	private int media;

	@Column(name="mention_id")
	private String mentionId;

	@Column(name="sentimen_tag")
	private int sentimenTag;

	private byte show;

	@Column(name="system_tag")
	private int systemTag;

	@Column(name="topic_id")
	private int topicId;

	private String url;

	public Mention() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFreeTag() {
		return this.freeTag;
	}

	public void setFreeTag(String freeTag) {
		this.freeTag = freeTag;
	}

	public int getInday() {
		return this.inday;
	}

	public void setInday(int inday) {
		this.inday = inday;
	}

	public int getMedia() {
		return this.media;
	}

	public void setMedia(int media) {
		this.media = media;
	}

	public String getMentionId() {
		return this.mentionId;
	}

	public void setMentionId(String mentionId) {
		this.mentionId = mentionId;
	}

	public int getSentimenTag() {
		return this.sentimenTag;
	}

	public void setSentimenTag(int sentimenTag) {
		this.sentimenTag = sentimenTag;
	}

	public byte getShow() {
		return this.show;
	}

	public void setShow(byte show) {
		this.show = show;
	}

	public int getSystemTag() {
		return this.systemTag;
	}

	public void setSystemTag(int systemTag) {
		this.systemTag = systemTag;
	}

	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}