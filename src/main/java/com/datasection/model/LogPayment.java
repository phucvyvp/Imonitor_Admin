package com.datasection.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the log_payment database table.
 * 
 */
@Entity
@Table(name="log_payment")
@NamedQuery(name="LogPayment.findAll", query="SELECT l FROM LogPayment l")
public class LogPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int id;

	private String coupon;

	@Lob
	private String description;

	private int during;

	private int flag;

	private int inday;

	@Column(name="plan_id")
	private int planId;

	@Column(name="user_id")
	private int userId;

	@Column(name="user_name")
	private String userName;

	public LogPayment() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCoupon() {
		return this.coupon;
	}

	public void setCoupon(String coupon) {
		this.coupon = coupon;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDuring() {
		return this.during;
	}

	public void setDuring(int during) {
		this.during = during;
	}

	public int getFlag() {
		return this.flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public int getInday() {
		return this.inday;
	}

	public void setInday(int inday) {
		this.inday = inday;
	}

	public int getPlanId() {
		return this.planId;
	}

	public void setPlanId(int planId) {
		this.planId = planId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}