package com.datasection.controller;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.boot.logging.java.SimpleFormatter;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.datasection.model.User;
import com.datasection.service.UserService;

@Controller
public class MyController {

	@Autowired
	private UserService userService;

	// @GetMapping(value="/home")
	// public String home2() {
	// return "admin/home";
	// }

	@GetMapping(value = "/")
	public String home() {
		return "redirect:/admin/home";
	}

	// @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	// public ModelAndView home(Model model,HttpServletRequest request,
	// RedirectAttributes redirect) {
	// ModelAndView modelAndView = new ModelAndView();
	// Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	//
	//
	// User user = userService.findUserByEmail(auth.getName());
	// modelAndView.addObject("userName",
	// "Welcome " + user.getName() + " " + " (" + user.getEmail() + ")");
	// //modelAndView.addObject("adminMessage", "Content Available Only for Users
	// with Admin Role");
	// modelAndView.setViewName("admin/home");
	//
	//
	//
	//
	// //model.addAttribute("users", userService.findAll());
	//
	//
	// return modelAndView;
	// }

	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public String index(Model model, HttpServletRequest request, RedirectAttributes redirect) {

		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		User user = userService.findUserByEmail(auth.getName());
		modelAndView.addObject("userName", "Welcome " + user.getName() + " " + " (" + user.getEmail() + ")");
		// modelAndView.addObject("adminMessage", "Content Available Only for Users with
		// Admin Role");
		modelAndView.setViewName("admin/home");

		request.getSession().setAttribute("userlist", null);
		if (model.asMap().get("success") != null)
			redirect.addFlashAttribute("success", model.asMap().get("success").toString());
		return "redirect:/admin/home/page/1";

		// model.addAttribute("users", userService.findAll());
		// return "admin/home";
	}

	// phân trang
	@GetMapping(value = "/admin/home/page/{pageNumber}")
	public String showTopicPage(HttpServletRequest request, @PathVariable int pageNumber, Model model) {
		PagedListHolder<?> pages = (PagedListHolder<?>) request.getSession().getAttribute("userlist");
		int pagesize = 100;
		List<User> list = (List<User>) userService.findAll();
		System.out.println(list.size());
		if (pages == null) {
			pages = new PagedListHolder<>(list);
			pages.setPageSize(pagesize);
		} else {
			final int goToPage = pageNumber - 1;
			if (goToPage <= pages.getPageCount() && goToPage >= 0) {
				pages.setPage(goToPage);
			}
		}
		request.getSession().setAttribute("userlist", pages);
		int current = pages.getPage() + 1;
		int begin = Math.max(1, current - list.size());
		int end = Math.min(begin + 30, pages.getPageCount());
		int totalPageCount = pages.getPageCount();

		String baseUrl = "/admin/home/page/";
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);
		model.addAttribute("totalPageCount", totalPageCount);
		model.addAttribute("baseUrl", baseUrl);
		model.addAttribute("users", pages);

		return "admin/home";
	}

	// tao moi
	@GetMapping(value = "/admin/home/create")
	public String createUser(Model model) {
		model.addAttribute("adduser", new User());

		return "user";
	}

	@PostMapping(value = "/admin/home/save")
	public String saveUser(@Valid User user) {
		Date date = new Date();
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//System.out.println(user.getExpireTime());
		user.setExpireTime(new Timestamp(user.getExpireTime().getTime()));
		user.setCreatedTime(new Timestamp(date.getTime()));
		userService.saveUser(user);

		return "redirect:/admin/home";
	}

	@GetMapping(value = "/admin/home/edit/{id}")
	public String editUser(@PathVariable int id, Model model) {

		model.addAttribute("adduser", userService.findUserById(id));
		return "useredit";
	}

	@GetMapping(value = "/admin/home/delete/{id}")
	public String deleteUser(@PathVariable int id, RedirectAttributes redirect) {
		userService.deleteUser(id);
		redirect.addFlashAttribute("Success!", "Delete user successfully!");
		return "redirect:/admin/home";
	}

	// search
	@GetMapping(value = "/admin/home/search/{pageNumber}")
	public String search(@RequestParam("usernames") String usernames, Model model, HttpServletRequest request,
			@PathVariable int pageNumber) {
		if (usernames.equals("")) {
			return "redirect:/admin/home";
		}
		List<User> list = userService.search(usernames);
		if (list == null) {
			return "redirect:/admin/home";
		}
		PagedListHolder<?> pages = (PagedListHolder<?>) request.getSession().getAttribute("userlist");
		int pagesize = 100;
		pages = new PagedListHolder<>(list);
		pages.setPageSize(pagesize);
		final int goToPage = pageNumber - 1;
		if (goToPage <= pages.getPageCount() && goToPage >= 0) {
			pages.setPage(goToPage);
		}
		request.getSession().setAttribute("userlist", pages);
		int current = pages.getPage() + 1;
		int begin = Math.max(1, current - list.size());
		int end = Math.min(begin + 40, pages.getPageCount());
		int totalPageCount = pages.getPageCount();

		String baseUrl = "/admin/home/page/";
		model.addAttribute("beginIndex", begin);
		model.addAttribute("endIndex", end);
		model.addAttribute("currentIndex", current);
		model.addAttribute("totalPageCount", totalPageCount);
		model.addAttribute("baseUrl", baseUrl);
		model.addAttribute("users", pages);

		return "admin/home";

		// model.addAttribute("users", userService.search(usernames));
		// return "admin/home";
	}

	// @GetMapping(value="/admin/home/topic")
	// public String indexTopic() {
	// return "topic";
	// }

}
