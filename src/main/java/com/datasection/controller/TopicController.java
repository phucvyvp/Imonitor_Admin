package com.datasection.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.datasection.model.Topic;
import com.datasection.service.TopicService;

@Controller
public class TopicController {

	@Autowired
	 private TopicService topicService;
	
	
	@GetMapping(value="/admin/home/topic")
	public String indexTopic(Model model) {
		
		model.addAttribute("topics", topicService.findAll());
		return "topic";
	}
	
	@GetMapping(value="/admin/home/topic/create")
	public String createTopic(Model model) {
		model.addAttribute("topics", new Topic());
		return "newtopic";
	}
	@PostMapping(value="/admin/home/topic/save")
	public String saveTopic(@Valid Topic topic ) {
		topicService.save(topic);
		return "redirect:/admin/home/topic";
	}
	
	@GetMapping(value="/admin/home/topic/edit/{id}")
	public String editUser(@PathVariable int id,Model model) {
		
		model.addAttribute("adduser",topicService.findTopicById(id));
		return "newtopic";
	}
	@GetMapping(value="/admin/home/topic/delete/{id}")
	public String deleteUser(@PathVariable int id,RedirectAttributes redirect) {
		topicService.delete(id);
		redirect.addFlashAttribute("Success!", "Delete user successfully!");
		return "redirect:/admin/home/topic";
	}
}
