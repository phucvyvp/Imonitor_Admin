package com.datasection.controller;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.datasection.model.User;
import com.datasection.service.UserService;

@Controller
public class LoginController {

	// private Logger logger=Logger.getLogger(this.getClass().getSimpleName());

	@Autowired
	private UserService userService;

	// @GetMapping(value = "/index")
	// public String index(Model model) {
	// model.addAttribute("users", userService.findAll());
	// return "index";
	// }
	
//	@GetMapping (value ="/")
//	public String home() {
//		return "redirect:/admin/home";
//	}
	@RequestMapping(value =  "/login" , method = RequestMethod.GET)
	public ModelAndView login() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("login");
		return modelAndView;
	}

	@RequestMapping(value = "/registration", method = RequestMethod.GET)
	public ModelAndView registration() {
		ModelAndView modelAndView = new ModelAndView();
		User user = new User();
		modelAndView.addObject("users", user);
		modelAndView.setViewName("registration");
		return modelAndView;
	}

	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) {
		ModelAndView modelAndView = new ModelAndView();
		User userExists = userService.findUserByEmail(user.getEmail());
		if (userExists != null) {
			bindingResult.rejectValue("email", "error.user",
					"There is already a user registered with the email provided");
		}
		if (bindingResult.hasErrors()) {
			modelAndView.setViewName("registration");
		} else {
		
			userService.saveUser(user);
			modelAndView.addObject("successMessage", "User has been registered successfully");
			modelAndView.addObject("users", new User());
			modelAndView.setViewName("registration");

		}
		return modelAndView;
	}

//	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
//	public ModelAndView home(Model model) {
//		ModelAndView modelAndView = new ModelAndView();
//		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		
//		
//		User user = userService.findUserByEmail(auth.getName());
//		modelAndView.addObject("userName",
//				"Welcome " + user.getName() + " "  + " (" + user.getEmail() + ")");
//		//modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
//		modelAndView.setViewName("admin/home");
//		
//		model.addAttribute("users", userService.findAll());
//		
//		
//		return modelAndView;
//	}
//
//
//	
//	// tao moi
//	@GetMapping(value = "/admin/home/create")
//	public String createUser(Model model) {
//		model.addAttribute("adduser", new User());
//		return "user";
//	}
//
//	@PostMapping(value = "/admin/home/save")
//	public String saveUser(@Valid User user) {
//		
//		userService.saveUser(user);
//		return "redirect:/admin/home";
//	}
//	@GetMapping(value="/admin/home/edit/{id}")
//	public String editUser(@PathVariable int id,Model model) {
//		
//		model.addAttribute("adduser",userService.finfUserById(id) );
//		return "user";
//	}
//	@GetMapping(value="/admin/home/delete/{id}")
//	public String deleteUser(@PathVariable int id,RedirectAttributes redirect) {
//		userService.deleteUser(id);
//		redirect.addFlashAttribute("Success!", "Delete user successfully!");
//		return "redirect:/admin/home";
//	}
//	
//	//search
//	@GetMapping(value="/admin/home/search")
//	public String search(@RequestParam("usernames") String usernames, Model model ) {
//		if(usernames.equals("")) {
//			return "redirect:/admin/home";
//		}
//		model.addAttribute("users", userService.search(usernames));
//		return "admin/home";
//	}

}
