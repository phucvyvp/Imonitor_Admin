package com.datasection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImonitorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImonitorApplication.class, args);
	}
}
