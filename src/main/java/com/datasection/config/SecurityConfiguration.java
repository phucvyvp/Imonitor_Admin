package com.datasection.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return bCryptPasswordEncoder;
	};

	@Autowired
	private DataSource dataSource;

	// @Value("${spring.queries.users-query}")
	// private String usersQuery;
	//
	// @Value("${spring.queries.roles-query}")
	// private String rolesQuery;
	//
	// @Autowired
	// private BCryptPasswordEncoder bCryptPasswordEncoder;
	//
	@Autowired
	public void configure(AuthenticationManagerBuilder auth) throws Exception {

		String user = "SELECT u.username,u.`password`,u.`status` enabled FROM `user` u WHERE u.username = ? LIMIT 1;";
		String sqlAuth = "SELECT u.username,r.role_name as role " + "FROM `user` u, user_role ur, role r "
				+ "WHERE u.id = ur.user_id AND r.id = ur.role_id and u.username=? and r.`status`=1 and ur.`status`=1;";
		//System.out.println("abc");
		auth.jdbcAuthentication().usersByUsernameQuery(user).authoritiesByUsernameQuery(sqlAuth).dataSource(dataSource)
				.passwordEncoder(passwordEncoder());
	}
	// @Override
	// protected void configure(AuthenticationManagerBuilder auth)
	// throws Exception {
	// auth.
	// jdbcAuthentication()
	// .usersByUsernameQuery(usersQuery)
	// .authoritiesByUsernameQuery(rolesQuery)
	// .dataSource(dataSource)
	// .passwordEncoder(bCryptPasswordEncoder);
	// }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		/*
		 * http.authorizeRequests().antMatchers("/").permitAll().antMatchers("/login").
		 * permitAll().antMatchers("/user/**")
		 * .hasAuthority("ADMIN").anyRequest().authenticated().and().csrf().disable().
		 * formLogin()
		 * .loginPage("/login").failureUrl("/login?error=true").defaultSuccessUrl(
		 * "/index")
		 * .usernameParameter("email").passwordParameter("password").and().logout()
		 * .logoutRequestMatcher(new
		 * AntPathRequestMatcher("/logout")).logoutSuccessUrl("/").and()
		 * .exceptionHandling().accessDeniedPage("/access-denied");
		 */
		http.authorizeRequests().antMatchers("/images/**").permitAll().antMatchers("/css/**").permitAll().antMatchers("/").permitAll().antMatchers("/home")
				.permitAll().antMatchers("/login").permitAll().antMatchers("/registration").permitAll()
				.antMatchers("/admin/**").hasAuthority("ADMIN").anyRequest().authenticated().and().csrf().disable()
				.formLogin().loginPage("/login").failureUrl("/login?error=true").defaultSuccessUrl("/admin/home")
				.usernameParameter("email").passwordParameter("password").and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/login").and()
				.exceptionHandling().accessDeniedPage("/access-denied");
	}

}
