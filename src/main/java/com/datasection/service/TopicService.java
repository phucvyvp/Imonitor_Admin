package com.datasection.service;

import com.datasection.model.Topic;

public interface TopicService {

	public Iterable<Topic> findAll();
	
	public Topic findTopicById(int id);
	void save(Topic topic);

	void delete(int id);
}
