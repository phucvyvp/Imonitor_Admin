package com.datasection.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datasection.model.Topic;
import com.datasection.repository.TopicRepository;
import com.datasection.service.TopicService;

@Service

public class TopicServiceImpl implements TopicService{

	@Autowired
	TopicRepository topicRepository;
	
	@Override
	public Iterable<Topic> findAll() {
		// TODO Auto-generated method stub
		return topicRepository.findAll();
	}

	@Override
	public Topic findTopicById(int id) {
		// TODO Auto-generated method stub
		return topicRepository.findTopicById(id);
	}

	@Override
	public void save(Topic topic) {
		// TODO Auto-generated method stub
		topicRepository.save(topic);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		topicRepository.deleteById(id);
	}

}
