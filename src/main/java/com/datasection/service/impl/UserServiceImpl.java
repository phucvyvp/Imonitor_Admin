package com.datasection.service.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.datasection.model.Role;
import com.datasection.model.User;
import com.datasection.repository.RoleRepository;
import com.datasection.repository.UserRepository;
import com.datasection.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	//@Override
	public User findUserByEmail(String email) {
		// TODO Auto-generated method stub
		return userRepository.findByEmail(email);
	}

	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setStatus(1);
 		Role userRole=roleRepository.findByRoleName("USER");
		user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));// ERROR không áp dung đối số kiểu hashset
		userRepository.save(user);
	}

	@Override
	public Iterable<User> findAll() {
		
		return userRepository.findAll();
	}

	@Override
	public User findUserById(int id) {
		// TODO Auto-generated method stub
		return userRepository.findUSerById(id);
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		userRepository.deleteById( id);
		
	}

	@Override
	public List<User> search(String usernames) {
		// TODO Auto-generated method stub
		return userRepository.findUserByUsername(usernames);
	}
	

}
