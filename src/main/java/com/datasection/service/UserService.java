package com.datasection.service;

import java.util.List;

import com.datasection.model.User;

public interface UserService {
	public Iterable<User> findAll();

	public User findUserByEmail(String email);

	public void saveUser(User user);

	public User findUserById(int id);

	public void deleteUser(int id);
	public List<User> search (String usernames);
}
